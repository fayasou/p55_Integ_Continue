package fr.afti.expression;

/**
 * 
 */
import static org.junit.Assert.*;

import org.junit.After;

import org.junit.Before;
import org.junit.Test;

/**
 *
 */
public class ExpressionTest {


	Expression e1;
	
	@Before
	public void setUp() throws Exception {
		e1 = new Expression("23 41 +");
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testConst() {
		String s = "32 5 -";
		Expression e = new Expression(s);
		assertEquals("erreur construction d'expression à partir d'une chaine", s, e.getValeur());
	
	}
	
	@Test
	public void testNext() {
		assertEquals("erreur 1er appel de next", "23", e1.next());
		assertEquals("erreur 2ème appel de next", "41", e1.next());
		assertEquals("erreur 3ème appel de next", "+", e1.next());
		assertEquals("erreur 4ème appel de next", "", e1.next());
	
	}
	
	@Test
	public void testEval() {
		assertEquals("erreur 1er appel de eval sur e1", 64, e1.eval());
		assertEquals("erreur 2eme appel de eval sur e1", 64, e1.eval());
	}
}
